/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8125, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "17 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Open Risk Management"], "isController": true}, {"data": [1.0, 500, 1500, "Submit Risk without File"], "isController": true}, {"data": [0.0, 500, 1500, "Submit Risk"], "isController": true}, {"data": [1.0, 500, 1500, "Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "20 /logout.php"], "isController": false}, {"data": [1.0, 500, 1500, "2 /index.php"], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "Submit Risk with File Attachment"], "isController": true}, {"data": [1.0, 500, 1500, "18 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "16 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "14 /reports"], "isController": false}, {"data": [1.0, 500, 1500, "21 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "19 /api/management/risk/viewhtml"], "isController": false}, {"data": [0.0, 500, 1500, "13 /index.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 90, 0, 0.0, 692.3555555555553, 9, 6294, 5923.5, 6001.05, 6294.0, 0.367045811395549, 2.4564469239012077, 0.6839055955624161], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["17 /management/index.php", 10, 0, 0.0, 41.5, 35, 55, 54.300000000000004, 55.0, 55.0, 0.04527140205532165, 0.3078322708701164, 0.28032016784372316], "isController": false}, {"data": ["Open Risk Management", 10, 0, 0.0, 23.2, 18, 52, 49.000000000000014, 52.0, 52.0, 0.04527365661742402, 0.3041868016538467, 0.01909982388547576], "isController": true}, {"data": ["Submit Risk without File", 10, 0, 0.0, 62.2, 56, 72, 71.4, 72.0, 72.0, 0.045268123093080315, 1.5118978420119868, 0.28333247512516635], "isController": true}, {"data": ["Submit Risk", 10, 0, 0.0, 6231.2, 6108, 6518, 6498.5, 6518.0, 6518.0, 0.04400285138476973, 2.650394011156923, 0.7379028942325463], "isController": true}, {"data": ["Open Login Page", 10, 0, 0.0, 23.200000000000003, 9, 147, 133.30000000000007, 147.0, 147.0, 0.045254397596086404, 0.06058962803147896, 0.019321506473641576], "isController": true}, {"data": ["20 /logout.php", 10, 0, 0.0, 25.3, 22, 39, 37.800000000000004, 39.0, 39.0, 0.04527509145568474, 0.08555400582690427, 0.03749343511173892], "isController": false}, {"data": ["2 /index.php", 10, 0, 0.0, 23.200000000000003, 9, 147, 133.30000000000007, 147.0, 147.0, 0.04526914771775592, 0.060609376485393905, 0.019327804084182505], "isController": false}, {"data": ["Login", 10, 0, 0.0, 6043.2, 5939, 6316, 6296.6, 6316.0, 6316.0, 0.044079466462137944, 0.38585029841798796, 0.09861489230284357], "isController": true}, {"data": ["Submit Risk with File Attachment", 10, 0, 0.0, 41.5, 35, 55, 54.300000000000004, 55.0, 55.0, 0.04527119710626508, 0.3078308772765753, 0.280318898800766], "isController": true}, {"data": ["18 /management/index.php", 10, 0, 0.0, 35.9, 32, 40, 39.8, 40.0, 40.0, 0.045273861588750346, 0.3078224673235904, 0.2653738008085912], "isController": false}, {"data": ["16 /management/index.php", 10, 0, 0.0, 23.2, 18, 52, 49.000000000000014, 52.0, 52.0, 0.04527345164795364, 0.30418542449520103, 0.019099737413980442], "isController": false}, {"data": ["Logout", 10, 0, 0.0, 38.0, 32, 62, 59.80000000000001, 62.0, 62.0, 0.04527283675067796, 0.14589879030980202, 0.055795234354839435], "isController": true}, {"data": ["14 /reports", 10, 0, 0.0, 20.5, 19, 22, 22.0, 22.0, 22.0, 0.045273246680339184, 0.15499897286321593, 0.03718242232242701], "isController": false}, {"data": ["21 /index.php", 10, 0, 0.0, 12.600000000000001, 10, 23, 22.200000000000003, 23.0, 23.0, 0.04527816641537284, 0.06035614956736712, 0.018305821187465193], "isController": false}, {"data": ["19 /api/management/risk/viewhtml", 10, 0, 0.0, 26.3, 23, 32, 31.8, 32.0, 32.0, 0.045274476513865305, 1.204283389926429, 0.01799483588002264], "isController": false}, {"data": ["13 /index.php", 10, 0, 0.0, 6022.7, 5919, 6294, 6274.7, 6294.0, 6294.0, 0.04408374147530649, 0.23496117599992947, 0.062418961684616095], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 90, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
